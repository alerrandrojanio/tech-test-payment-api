using System.ComponentModel;

namespace src.Models
{
    public enum EnumStatusEntrega {

        [DescriptionAttribute("Aguardando pagamento")]
        AguardandoPagamento,
        [DescriptionAttribute("Pagamento aprovado")]
        PagamentoAprovado,
        [DescriptionAttribute("Enviado para transportadora")]
        EnviadoTransportadora,
        Entregue,	
        Cancelada  
    }
}