namespace src.Models
{
    public class Produto {

        public Produto(string nome){
            Nome = nome;
        }
        public int Id { get; set; }
        public string Nome { get; set; } 
        
    }
}