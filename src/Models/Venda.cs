namespace src.Models
{
    public class Venda {

        public Venda(){
            
        }
        
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public List<Produto> Produtos { get; set; }
        public EnumStatusEntrega Status { get; set; }
        
    }
}