using Microsoft.EntityFrameworkCore;
using src.Models;

namespace src.Context;

public class DatabaseContext : DbContext {

    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options){

        
    }
    public DbSet<Venda> Vendas { get; set; }
    public DbSet<Produto> Produtos { get; set; }
    public DbSet<Vendedor> Vendedores { get; set; }


    protected override void OnModelCreating(ModelBuilder builder){

        builder.Entity<Venda>(tabela => {
            tabela.HasKey(e => e.Id);
        });

        builder.Entity<Produto>(tabela => {
            tabela.HasKey(e => e.Id);
        });

        builder.Entity<Vendedor>(tabela => {
            tabela.HasKey(e => e.Id);
        });

    }

}