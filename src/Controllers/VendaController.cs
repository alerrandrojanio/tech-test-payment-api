using System.Net;
using Microsoft.AspNetCore.Mvc;
using src.Context;
using src.Models;

namespace src.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase {
        private DatabaseContext _context { get; set; }

        public VendaController(DatabaseContext context){
            this._context = context;
        }

        [HttpGet("ListarVendas")]
        public ActionResult<List<Venda>> ListarVendas(){
            var result = _context.Vendas.ToList();

            if(!result.Any())
                return NoContent();
            
            return Ok(result);
        }

        [HttpGet("ListaVendaPorId")]
        public ActionResult<List<Venda>> ListaVendaPorId(int id){
            var result = _context.Vendas.SingleOrDefault(e => e.Id == id);

            if(result == null)
                return NoContent();
            
            return Ok(result);
        }

        [HttpPost("CriarVenda")]
        public ActionResult CriarVenda(Venda venda) {
            try{
                if(!venda.Produtos.Any()){
                    return BadRequest(new {
                        msg = "Deve haver ao menos um produto!",
                        status = HttpStatusCode.BadRequest
                    });
                }
                else {
                    venda.Status = EnumStatusEntrega.AguardandoPagamento;

                    _context.Vendas.Add(venda);
                    _context.SaveChanges();
                }
        
            } 
            catch(SystemException){
                return BadRequest(new {
                    msg = "Houve um erro ao tentar criar venda!",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Created("Venda criado!", venda);
        }

        [HttpPut("AtualizarStatusEntrega")]
        public ActionResult AtualizarStatusEntrega(int id, bool cancelar) {
            var venda = _context.Vendas.SingleOrDefault(e => e.Id == id);

            if(venda is null)
                return NotFound(new {
                    msg = "Venda não encontrada!",
                    status = HttpStatusCode.NotFound
                });

            try {
                if(cancelar) {
                    if(venda.Status == EnumStatusEntrega.AguardandoPagamento || venda.Status == EnumStatusEntrega.PagamentoAprovado)
                        venda.Status = EnumStatusEntrega.Cancelada;
                    else {
                        return Unauthorized(new {
                            msg = "Impossivel cancelar!",
                            status = HttpStatusCode.Unauthorized
                        });
                    }
                }
                else{
                    if(venda.Status == EnumStatusEntrega.AguardandoPagamento)
                    venda.Status = EnumStatusEntrega.PagamentoAprovado;

                    else if (venda.Status == EnumStatusEntrega.PagamentoAprovado)
                        venda.Status = EnumStatusEntrega.EnviadoTransportadora;

                    else if (venda.Status == EnumStatusEntrega.EnviadoTransportadora)
                        venda.Status = EnumStatusEntrega.Entregue;
                    else
                        throw new SystemException();
                }

                _context.Vendas.Update(venda);
                _context.SaveChanges();

            }
            catch(SystemException){
                return BadRequest(new {
                    msg = "Houve um erro ao atualizar status!",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Ok(new {
                msg = "Status da entrega " + id + " atualizado!\n",
                status = HttpStatusCode.OK
            });  
        }
    }
}