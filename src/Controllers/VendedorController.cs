using Microsoft.AspNetCore.Mvc;
using src.Context;
using src.Models;
using System.Net;

namespace src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase {
        private DatabaseContext _context { get; set; }

        public VendedorController(DatabaseContext context){
            this._context = context;
        }

        [HttpGet("ListarVendedores")]
        public ActionResult<List<Vendedor>> ListarVendedores() {
            var result = _context.Vendedores.ToList();

            if(!result.Any())
                return NoContent();

            return Ok(result);
        }

        [HttpPost("CriarVendedor")]
        public ActionResult<Vendedor> CriarVendedor([FromBody] Vendedor vendedor) {
            try {
                _context.Vendedores.Add(vendedor);
                _context.SaveChanges();
            } 
            catch(SystemException) {
                return BadRequest(new
                {
                    msg = "Houve um erro ao tentar criar vendedor!",
                    status = HttpStatusCode.BadRequest
                });
            }
            
            return Created("Produto criado!", vendedor);
        }
        
    }
}