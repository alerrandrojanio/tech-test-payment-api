using Microsoft.AspNetCore.Mvc;
using src.Context;
using src.Models;
using System.Net;

namespace src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase {
        private DatabaseContext _context { get; set; }

        public ProdutoController(DatabaseContext context){
            this._context = context;
        }

        [HttpGet("ListarProdutos")]
        public ActionResult<List<Produto>> ListarProdutos(){
            var result = _context.Produtos.ToList();

            if(!result.Any())
                return NoContent();
            
            return Ok(result);
        }

        [HttpPost("CriarProduto")]
        public ActionResult<Produto> CriarProduto([FromBody]Produto produto){
            try{
                _context.Produtos.Add(produto);
                _context.SaveChanges();
        
            } 
            catch(SystemException) {
                return BadRequest(new {
                    msg = "Houve um erro ao tentar criar produto!",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Created("Produto criado!", produto);
        }
    }
}